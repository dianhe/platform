window.addEventListener('touchmove', function(){}, { passive: false })

let selectaddressValue = $('#selectaddress').val()
$('#selectaddress').picker({
  title: "请选择地域",
  toolbarCloseText:'完成',
  // rotateEffect:true,
  cols: [
    {
      textAlign: 'center',
      values: ['睢县', '虞城', '民权', '柘城', '宁陵']
    }
  ],
  onChange:function(){
    selectaddressValue = $('#selectaddress').val()
  },
  onClose:function(){
    //可以在这里请求方法
    if($('#selectaddress').val()!=selectaddressValue){
      //这里请求修改的接口
      console.log('修改了')
    }
  }
});